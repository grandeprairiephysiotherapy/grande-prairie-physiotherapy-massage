We are Physiotherapy & Massage therapy clinic located in Grande Prairie and Peace River, Alberta.
Our experienced therapists are here to help you. Call us about our many effective services, including advanced laser therapy, shock-wave therapy, spinal manipulation, and functional dry needling.

Address: 9725 98 Ave, Unit 103, Grande Prairie, AB T8V 8K5, Canada

Phone: 780-357-0095

Website: https://grandeprairiephysiotherapy.com
